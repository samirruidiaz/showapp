import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import VModal from 'vue-js-modal';

Vue.use(VModal);
Vue.config.productionTip = false

// Importing the global scss file
//import "@/assets/css/main.scss"


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
