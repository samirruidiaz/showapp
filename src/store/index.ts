import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    listData: [],
    listGenders: [],
    listGendersMovies: [],
    listGendersTv: [],
    linkYoutube: ''
  },
  mutations: {
    getDataMutations(state, arrayData){
      state.listData = arrayData
    },
    getDataGenders(state, arrayDataGenders){
      state.listGenders = arrayDataGenders
    },
    getDataContentGendersMovies(state, arrayDataContentGenders){
      state.listGendersMovies = arrayDataContentGenders
    },
    getDataContentGendersTv(state, arrayDataContentGenders){
      state.listGendersTv = arrayDataContentGenders
    },
    getDataLink(state, LinkData){
      state.linkYoutube = LinkData
    }
  },
  actions: {
    async getData({ commit }, params){
      const response = await fetch(`${process.env.VUE_APP_DOMINIO}/discover/${params.type}?api_key=${process.env.VUE_APP_API_KEY}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1`);
      if (response.ok) {
        const data = await response.json();
        commit('getDataMutations', data)
      } else {
        alert("HTTP-Error: " + response.status);
      }
    },
    async searchMovie({ dispatch, commit }, params){
      if (params.search.length > 0) {
        const response = await fetch(`${process.env.VUE_APP_DOMINIO}/search/${params.type}?api_key=${process.env.VUE_APP_API_KEY}&language=en-US&query=${params.search}&page=1&include_adult=false`);
        if (response.ok) {
          const data = await response.json();
          commit('getDataMutations', data);
        } else {
          alert("HTTP-Error: " + response.status);
        }
      } else {
        await dispatch('getData', params);
      }
    },
    async getListGenders({ commit }, params){
      const response = await fetch(`${process.env.VUE_APP_DOMINIO}/genre/${params.type}/list?api_key=${process.env.VUE_APP_API_KEY}&language=en-US`);
      if (response.ok) {
        const data = await response.json();
        commit('getDataGenders', data.genres);
      } else {
        alert("HTTP-Error: " + response.status);
      }
    },
    async getListGendersContentMovie({ commit }){
      const response = await fetch(`${process.env.VUE_APP_DOMINIO}/genre/movie/list?api_key=${process.env.VUE_APP_API_KEY}&language=en-US`);
      if (response.ok) {
        const data = await response.json();
        commit('getDataContentGendersMovies', data.genres);
      } else {
        alert("HTTP-Error: " + response.status);
      }
    },
    async getListGendersContentTv({ commit }){
      const response = await fetch(`${process.env.VUE_APP_DOMINIO}/genre/tv/list?api_key=${process.env.VUE_APP_API_KEY}&language=en-US`);
      if (response.ok) {
        const data = await response.json();
        commit('getDataContentGendersTv', data.genres);
      } else {
        alert("HTTP-Error: " + response.status);
      }
    },
    async openModal({ commit }, params){
      const response = await fetch(`${process.env.VUE_APP_DOMINIO}/${params.type}/${params.id}/videos?api_key=${process.env.VUE_APP_API_KEY}&language=en-US`);
      if (response.ok) {
        const data = await response.json();
        commit('getDataLink', ( data.results.length > 0 ) ? data.results[0].key : '');
      } else {
        alert("HTTP-Error: " + response.status);
      }
    },
    async searchYearGenders({ commit }, params){
      const resultGender = ( params.gendersId > 0 ) ? `&with_genres=${params.gendersId}` : '';
      const response = await fetch(`${process.env.VUE_APP_DOMINIO}/discover/${params.type}?api_key=${process.env.VUE_APP_API_KEY}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&year=${params.year}${resultGender}`);
      if (response.ok) {
        let data = await response.json();
        commit('getDataMutations', data);
      } else {
        alert("HTTP-Error: " + response.status);
      }
    },

  },
  modules: {
  }
})
